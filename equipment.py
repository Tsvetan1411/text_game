import commands


class Equipment:
    equipment_dict = {}
    def __init__(self, name, location, can_take):
        self.name = name
        self.location = location
        self.can_take = can_take
        self.equipment_dict[self.name] = {"mem_place": self, "location": self.location, "can take": self.can_take}


class Door(Equipment):
    def __init__(self, name, location, can_take, leads_to, match, locked):
        self.leads_to = leads_to
        self.match = match
        self.locked = locked
        super().__init__(name, location, can_take)

    def use(self, equipment_dict, current_room):
        if self.locked:
            print("***" * 10)
            print("Drzwi są zablokowane. Masz przy sobie: ")
            for items in equipment_dict:
                if equipment_dict[items]["location"] == "inventory":
                    print(items)
                    print("***" * 10)
            user_input = str(input("Czym chcesz otworzyć: ")).lower()
            if user_input == self.match:
                self.locked = False
            else:
                print("***" * 10)
                print("Tym nie da się otworzyć")
        else:
            equipment_dict[self.name]["location"] = self.leads_to
            self.leads_to = current_room
            current_room = equipment_dict[self.name]["location"]
            commands.get_describe(equipment_dict, current_room)
        return equipment_dict, current_room
            
class CodeBox(Equipment):
    def __init__(self, name, location, can_take, locked, code, contain):
        self.locked = locked
        self.code = code
        self.contain = contain
        super().__init__(name, location, can_take)

    def use(self, euquipment_dict, current_room):
        if self.locked:
            print("***" * 10)
            user_input = input("Wprowadź kod: ")
            if user_input == self.code:
                print("***" * 10)
                print(f"Kod poprawny. W środku znalazłeś {self.contain}")
                euquipment_dict[self.contain]["location"] = "inventory"
                self.locked = False
            else:
                print("***" * 10)
                "Kod niepoprawny"
        elif euquipment_dict[self.contain]["location"] == "inventory":
            print("***" * 10)
            print("Nic w środku nie ma")
        return euquipment_dict, current_room
    
class ControlPanel(Equipment):
    def __init__(self, name, location, can_take, locked, match, sub_depth):
        self.locekd = locked
        self.match = match
        self.sub_depth = sub_depth
        super().__init__(name, location, can_take)

    def use(self, equipment_dict, current_room):
        if self.locekd:
            print("***" * 10)
            print("Panel zablokowany. Masz przy sobie: ")
            for items in equipment_dict:
                if equipment_dict[items]["location"] == "inventory":
                    print(items)
            print("***" * 10)
            user_input = str(input("Czym chcesz odblokować: ")).lower()
            if user_input == self.match:
                print("***" * 10)
                print("Panel odblokowany")
                self.locekd = False
            else:
                print("***" * 10)
                print("Błędne polecenie")
        else:
            print("***" * 10)
            print(f"Aktualne zanurzenie łodzi: {self.sub_depth} m")
            user_input = input("Podaj żądaną głebokość zanurzenia: ")
            self.sub_depth = user_input
        return equipment_dict, current_room
    