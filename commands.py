import pathlib
import csv

def take(equipment_dict, current_room):
    equipment_list = [item for item in equipment_dict if equipment_dict[item]["location"] == current_room]
    while True:
        print("***" * 10)
        user_input = str(input("Co chcesz zabrać lub wpisz 'wyjdź': ")).lower()
        if user_input in equipment_list and equipment_dict[user_input]["can take"]:
            equipment_dict[user_input]["location"] = "inventory"
            break
        elif user_input == "wyjdź":
            break
        else:
            print("Błędne polecenie")
    return equipment_dict, current_room

def use(eqipment_dict, current_room):
    user_input = str(input("Czego chcesz uzyć: ")).lower()
    if user_input in eqipment_dict and eqipment_dict[user_input]["location"] == current_room:
        try:
            eqipment_dict, current_room = eqipment_dict[user_input]["mem_place"].use(eqipment_dict, current_room)
        except AttributeError:
            print("Błędne polecenie")
    else:
        print("Błędne polecenie")
    return eqipment_dict, current_room

def get_describe(equipemnt_dict, current_room):
    path = pathlib.Path.cwd() / (current_room + ".txt")
    with open(path, mode="r", encoding="utf-8") as file:
        print(file.read())
    for items in equipemnt_dict:
        if equipemnt_dict[items]["location"] == current_room:
            print(items)
    return equipemnt_dict, current_room

def get_help():
    print("***" * 10)
    print("Możesz użyć poleceń: weź, użyj, rozejrzyj się, pomoc, wyjdź")
    print("***" * 10)

def get_time_result(start_time, stop_time):
    result = stop_time - start_time
    minutes = int(result // 60)
    seconds = result % 60
    return f"min: {minutes} sec: {seconds:.2f}"

def write_result(name, time_result):
    result_dic = {"name": name, "time": time_result}
    path_result_csv = pathlib.Path.cwd() / "results.csv"
    if not path_result_csv.exists():
        path_result_csv.touch()
        with path_result_csv.open(mode="w", encoding="utf-8", newline="") as file:
            writer = csv.DictWriter(file, fieldnames=["name", "time"])
            writer.writeheader()
            writer.writerow(result_dic)
    else:
        with path_result_csv.open(mode="a", encoding="utf-8", newline="") as file:
            writer = csv.DictWriter(file, fieldnames=["name", "time"])
            writer.writerow(result_dic)

def get_results():
    print("***" * 10)
    print("Wyniki graczy:")
    path_result_csv = pathlib.Path.cwd() / "results.csv"
    with path_result_csv.open(mode="r", encoding="utf-8", newline="") as file:
            reader = csv.DictReader(file)
            for line in reader:
                print(f"Name: {line['name']} Time: {line['time']}")

            