import equipment
import commands
import time


door = equipment.Door("stalowe drzwi", "pomieszczenie", False, "mostek", "łom", True)
crowbar = equipment.Equipment("łom", "pomieszczenie", True)
box = equipment.CodeBox("czarna skrzynka", "mostek", False, True, "1893", "karta magnetyczna")
card = equipment.Equipment("karta magnetyczna", "czarna skrzynka", True)
control_panel = equipment.ControlPanel("panel kontrolny", "mostek", False, True, "karta magnetyczna", "1200")
dict_equip = equipment.Equipment.equipment_dict
current_room = "pomieszczenie"

commands.get_describe(dict_equip, "intro")
commands.get_help()
user_name = input("Podaj swoje imię: ")
commands.get_describe(dict_equip, current_room)
start_time = time.time()

while True:
    if control_panel.sub_depth == "0":
        print("Jesteś na powierzchni!!!")
        break
    print("***" * 10)
    user_input = str(input("Co chcesz zrobić: ")).lower()
    if user_input == "weź":
        dict_equip, current_room = commands.take(dict_equip, current_room)
    elif user_input == "użyj":
        dict_equip, current_room = commands.use(dict_equip, current_room)
    elif user_input == "rozejrzyj się":
        commands.get_describe(dict_equip, current_room)
    elif user_input == "pomoc":
        commands.get_help()
    elif user_input == "wyjdź":
        break
    else:
        print("Błędne polecenie")
    
stop_time = time.time()
time_result = commands.get_time_result(start_time, stop_time)
if control_panel.sub_depth == "0":
    commands.write_result(user_name, time_result)
    commands.get_results()